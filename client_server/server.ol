include "console.iol"
include "iface.iol"

execution { concurrent }

inputPort MyInput {
Location: "socket://localhost:8000/"
Protocol: sodep
Interfaces: ServerInterface
}

main
{
	sendNumber( x )( y ) {
		y = x + 2
	}
}
