include "console.iol"
include "iface.iol"

outputPort MyOutputPort {
Location: "socket://localhost:8000/"
Protocol: sodep
Interfaces: ServerInterface
}

main
{
	sendNumber@MyOutputPort( int( args[0] ) )( x );
	println@Console( x )()
}
